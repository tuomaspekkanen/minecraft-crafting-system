#ifndef STRINGMAPSET_HH
#define STRINGMAPSET_HH

#include "stringset.hh"

class StringMapSet
{
public:
    StringMapSet();
    ~StringMapSet();

    bool lisaa(const string& avain, const string& alkio);
    StringSet hae_edeltajat(const string& avain) const;
    bool sisaltaako(const string& avain) const;
    bool empty() const;

private:
    struct Alkio {
        string avain_;
        string* kuorma_;
        unsigned int koko_;
        unsigned int maxkoko_;
        Alkio* seuraava_alkio_;
    };
    Alkio* ensimmainen_alkio_;
};

#endif // STRINGMAPSET_HH

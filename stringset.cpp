#include "stringset.hh"

//rakentaja alustaa alkiolle tilaa ja luo sen.
StringSet::StringSet() :
    alkiot_{new string[20]}, koko_{0}, maxkoko_{20}
{}

//varattu tila pitaa lopuksi poistaa
StringSet::~StringSet()
{
    delete [] alkiot_;
}

//palauttaa alkioiden maaran tietorakenteessa
//paluuarvonaan.
unsigned int StringSet::size() const
{
    return koko_;
}

//kopioi vanhan tietorakenteen uuteen kaksi
//kertaa isompaan tietorakenteeseen.
void StringSet::lisaa_tilaa()
{
    string* uusiarray = new string[2*maxkoko_];
    for(unsigned int i = 0; i < koko_; ++i)
    {
        uusiarray[i] = alkiot_[i];
    }
    delete [] alkiot_;
    alkiot_ = uusiarray;
    maxkoko_ *= 2;
}

//lisaa tietorakenteeseen viimeiseksi jaseneksi
//parametrin alkion. huolehtii myos tilan varaamisesta,
//jos se on lopussa. tassa kohtaa ei varsinaisesti
//tehda set-rakenteen vaatimusta siita, etta alkio ei
//esiinny useasti rakenteessa, koska nain samaa rakennetta
//voi kayttaa myos vahan kuin vektoria. settia lahentelevan
//rakenteesta saa suorittamalla ensin sisaltaako-funktion
//avulla tarkistuksen alkion olemassaolosta.
void StringSet::lisaa(const string& alkio)
{
    if(koko_ == maxkoko_)
        lisaa_tilaa();
    alkiot_[koko_] = alkio;
    ++koko_;
}

//laittaa koko tietorakenteen alkiot
//aakkosjarjestykseen.
void StringSet::aakkosjarjestykseen()
{
    sort(alkiot_, alkiot_+koko_);
}

//tarkistaa sisaltaako tietorakenne parametrin nimisen alkion.
//jos sisaltaa, palautetaan true. muutoin palautetaan false.
bool StringSet::sisaltaako(const string& alkio) const
{
    for(unsigned int i = 0; i < koko_; ++i)
    {
        if(alkiot_[i] == alkio)
        {
            return true;
        }
    }
    return false;
}

//palauttaa halutun indeksin kohdassa olevan
//alkion tietorakenteesta. alkion muokkauksen
//voisi kieltaa palauttamalla esim. const string&
//mutta paatin sallia sen, mikali ohjelmaa joskus
//jatkokehitettaisiin ja se tarvisi tata ominaisuutta.
string& StringSet::at(int indeksi) const
{
    return alkiot_[indeksi];
}

//tulostaa kaikki tietorakenteen alkiot
//omille riveilleen.
void StringSet::tulosta() const
{
    for(unsigned int i = 0; i < koko_; ++i)
    {
        cout << alkiot_[i] << endl;
    }
}

//tyhjentaa tietorakenteen. tyhjennys ei varsinaisesti
//poista alkioita rakenteesta, vaan laittaa kasiteltavan
//alkion ensimmaiseksi ja koodi ei taten kayta vanhoja arvoja.
//nain jo varattu koko tietorakenteelle sailyy eika arvoja
//tarvitse turhaan poistaa tassa kohtaa, vaan vasta purkaja
//poistaa myos nama jaannosarvot, jos niita viela on.
void StringSet::tyhjenna()
{
    koko_ = 0;
}

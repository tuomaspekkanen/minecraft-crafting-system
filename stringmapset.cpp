#include "stringmapset.hh"

//rakentaja alustaa ensimmaisen alkion arvon
StringMapSet::StringMapSet() :
    ensimmainen_alkio_{nullptr}
{}

//purkaja poistaa ensimmaisesta alkiosta lahtien
//alkion sisallon ja sitten itse alkion, kunnes
//kaikki alkiot on poistettu sisaltoineen.
StringMapSet::~StringMapSet()
{
    Alkio* poistettava_alkio = ensimmainen_alkio_;
    while(ensimmainen_alkio_ != nullptr)
    {
        delete [] ensimmainen_alkio_->kuorma_;
        poistettava_alkio = ensimmainen_alkio_;
        ensimmainen_alkio_ = ensimmainen_alkio_->seuraava_alkio_;
        delete poistettava_alkio;
    }
}

//tarkistaa onko tietorakenne tyhja.
//palauttaa true jos tietorakenne ei sisalla
//alkioita, ja muutoin false
bool StringMapSet::empty() const
{
    if(ensimmainen_alkio_ == nullptr)
        return true;
    else
        return false;
}

//tarkistaa onko parametrin avain tietorakenteessa.
//palauttaa true jos on, muutoin false
bool StringMapSet::sisaltaako(const string& avain) const
{
    Alkio* nykyinen_alkio = ensimmainen_alkio_;
    while(nykyinen_alkio != nullptr)
    {
        if(nykyinen_alkio->avain_ == avain)
            return true;
        nykyinen_alkio = nykyinen_alkio->seuraava_alkio_;
    }
    return false;
}

//lisaa tietorakenteeseen avain-kuorma-parin. avaimella siis loytaa kuorman.
//kuorma voi sisaltaa useita alkioita, mutta funktio listaa vain yhden alkion
//kuormaan kerrallaan. lisays ei onnistu, jos sama avain-kuorma-pari on jo olemassa
//tietorakenteessa. talloin palautetaan false. jos lisays onnistuu parametrien
//avaimelle ja kuormalle, palautetaan true.
bool StringMapSet::lisaa(const string& avain, const string& kuorma)
{
    if(empty())
    {
        ensimmainen_alkio_ = new Alkio{avain, nullptr, 0, 0, nullptr};
    }

    Alkio* nykyinen_alkio = ensimmainen_alkio_;

    //tutkitaan, onko avain jo olemassa ja siirrytaan
    //sen alkioon. jos avainta ei ole, siirrytaan sailion loppuun
    while(true)
    {
        //avain loytyi. voidaan jatkaa.
        if(nykyinen_alkio->avain_ == avain) break;

        //avainta ei ole. taytyy luoda uusi avain
        if(nykyinen_alkio->seuraava_alkio_ == nullptr)
        {
            nykyinen_alkio->seuraava_alkio_ = new Alkio{avain, nullptr, 0, 0, nullptr};
            nykyinen_alkio = nykyinen_alkio->seuraava_alkio_;
            break;
        }
        //siirrytaan seuraavaan alkioon
        nykyinen_alkio = nykyinen_alkio->seuraava_alkio_;
    }

    //jos kuormaa ei ole, varataan sille tila
    if(nykyinen_alkio->kuorma_ == nullptr)
    {
        nykyinen_alkio->kuorma_ = new string[10];
        nykyinen_alkio->koko_ = 0;
        nykyinen_alkio->maxkoko_ = 10;
    }

    //jos kuorma on taysi, tuplataan sen (max)koko.
    if(nykyinen_alkio->koko_ == nykyinen_alkio->maxkoko_)
    {
        string* uusi_taulukko = new string[2 * nykyinen_alkio->maxkoko_];

        for(unsigned int i = 0; i < nykyinen_alkio->koko_; ++i)
        {
            uusi_taulukko[i] = nykyinen_alkio->kuorma_[i];
        }

        delete [] nykyinen_alkio->kuorma_;
        nykyinen_alkio->kuorma_ = uusi_taulukko;
        nykyinen_alkio->maxkoko_ *= 2;
    }

    //tarkistetaan onko kuorman alkio uniikki kyseiselle avaimelle
    //eli sita ei saa esiintya ennestaan
    for(unsigned int i = 0; i < nykyinen_alkio->koko_; ++i)
    {
        if(nykyinen_alkio->kuorma_[i] == kuorma)
            return false;
    }

    //nyt voidaan turvallisesti lisata kuorman alkio avaimelle
    nykyinen_alkio->kuorma_[nykyinen_alkio->koko_] = kuorma;
    nykyinen_alkio->koko_++;


    //tehdaan sama toimenpide kuin alussa avaimelle, mutta
    //talla kertaa kuormalle. Sille ei laiteta alkioita sisalle.
    nykyinen_alkio = ensimmainen_alkio_;

    while(true)
    {
        if(nykyinen_alkio->avain_ == kuorma) break;

        if(nykyinen_alkio->seuraava_alkio_ == nullptr)
        {
            nykyinen_alkio->seuraava_alkio_ = new Alkio{kuorma, nullptr, 0, 0, nullptr};
            break;
        }
        nykyinen_alkio = nykyinen_alkio->seuraava_alkio_;
    }

    return true;
}

//funktio hakee tietorakenteesta parametrina annetun avaimen kuorman
//kaikki alkiot, ja palauttaa ne StringSet-rakenteena.
StringSet StringMapSet::hae_edeltajat(const string& avain) const
{
    StringSet edeltajat;
    Alkio* nykyinen_alkio = ensimmainen_alkio_;

    //etsitaan avain rakenteesta
    while(nykyinen_alkio != nullptr)
    {
        //jos avain loytyy, lisataan sen koko kuorma
        //alkio kerrallaan aiemmin maariteltyyn stringsettiin.
        //jos avainta ei loydy, palautetaan sama stringsetti,
        //mutta se on tyhja.
        if(nykyinen_alkio->avain_ == avain)
        {
            for(unsigned int i = 0; i < nykyinen_alkio->koko_; ++i)
            {
                edeltajat.lisaa(nykyinen_alkio->kuorma_[i]);
            }
            break;
        }
        nykyinen_alkio = nykyinen_alkio->seuraava_alkio_;
    }

    //alkiot halutaan aakkosjarjestyksessa takaisin.
    edeltajat.aakkosjarjestykseen();
    return edeltajat;
}

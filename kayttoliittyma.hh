#ifndef KAYTTOLIITTYMA_HH
#define KAYTTOLIITTYMA_HH

#include "stringmapset.hh"

#include <iostream>
#include <string>
#include <regex>

void kayttoliittyma(StringMapSet& reseptiluettelo);

#endif // KAYTTOLIITTYMA_HH

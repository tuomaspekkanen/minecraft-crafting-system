Minecraftin tapainen reppu/craftaus-järjestelmä komentoriviltä ohjattuna.
Projekti on tehty QT Creatorilla ja tarkoituksena oli olla käyttämättä
C++:n STL-säiliöitä, vaan toteuttaa muistinhallinta itse.

Ohjelman aluksi kerrotaan tekstitiedosto, jonka sisällä ovat tunnetut esineiden
reseptit, eli valmistusohjeet kuvattuna seuraavasti:

esine:materiaali
esine:materiaali2
esine2:materiaali
...

Komennot:
loppu
-lopettaa ohjelman

reppu materiaali_1 materiaali_2 ... materiaali_n
-asettaa reppuun välilyönnein erotellut materiaalit. Mikäli materiaaleja
ei anneta, reppu asetetaan tyhjäksi.

tulostareppu
-tulostaa rivi kerrallaan repussa olevat materiaalit

askarreltavissa esine_tai_materiaali
-kertoo, onko kysytty esine mahdollista valmistaa repussa olevista
materiaaleista. Esine on askarreltavissa, jos sen suorat edeltäjät löytyvät
repusta.

Eli esimerkiksi jos seuraavat reseptit ovat olemassa:
rautalapio:rautaharkko
rautalapio:lapionvarsi
rautaharkko:rautaa

ja repussa on rautaharkko ja lapionvarsi, silloin rautalapio on askarreltavissa.
Jos repussa taas on esimerkiksi lapionvarsi ja rautaa, rautalapio ei ole
askarreltavissa. Jos repussa on jo valmiina rautalapio, se on askarreltavissa.


materiaalit esine_tai_materiaali
-tulostaa aakkosjärjestyksessä kaikki materiaalit, jotka tarvitaan
ilmoitetun esineen askartelemiseen.

askarreltavissa_bonus esine_tai_materiaali
-sama kuin askarreltavissa, mutta laskee myös edelliset jäsenet.
eli erona askarreltavissa-komennon esimerkkiin olisi se, että rautalapio olisi
askarreltavissa myös silloin, kun repussa on vain rautaa ja lapionvarsi.


Virheellisistä syötteistä ilmoitetaan seuraavasti:

Virhe: virheilmoitus

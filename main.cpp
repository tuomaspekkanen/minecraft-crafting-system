#include "stringmapset.hh"
#include "kayttoliittyma.hh"
#include "tiedostonluku.hh"
#include "stringset.hh"

#include <iostream>
#include <fstream>

using namespace std;

//aloittaa tiedoston lukemisen ja mikali kaikki onnistuu
//vie reseptiluettelon kasiteltavaksi kayttoliittymalle.
//palauttaa arvon 1 jos on tapahtunut virhe. muutoin
//ei palauta mitaan.
int main()
{
    StringMapSet reseptiluettelo;
    string tiedoston_nimi;
    cout << "Syotetiedosto: ";
    getline(cin, tiedoston_nimi);

    ifstream virta(tiedoston_nimi);
    if(!virta)
    {
        cout << "Virhe: tiedostoa ei voitu lukea." << endl;
        return 1;
    }
    else if(!lue_tehtavatiedosto(virta, reseptiluettelo))
    {
        cout << "Virhe: tiedosto ei ole vaaditun muotoinen." << endl;
        return 1;
    }
    else
    {
        virta.close();
        cout << "Tuotantoketjut luettu onnistuneesti." << endl;
        kayttoliittyma(reseptiluettelo);
    }
}

#ifndef STRINGSET_HH
#define STRINGSET_HH

#include <iostream>
#include <algorithm>

using namespace std;

class StringSet
{
public:
    StringSet();
    ~StringSet();

    void lisaa(const string& alkio);
    bool sisaltaako(const string& alkio) const;
    string& at(int indeksi) const;
    void aakkosjarjestykseen();
    void tulosta() const;
    void tyhjenna();
    unsigned int size() const;
    void lisaa_tilaa();

private:
    string* alkiot_;
    unsigned int koko_;
    unsigned int maxkoko_;
};

#endif // STRINGSET_HH

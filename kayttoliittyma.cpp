#include "kayttoliittyma.hh"

void reppu(StringSet& materiaalit, const StringMapSet& reseptiluettelo, const StringSet& kentat);
void tulostareppu(const StringSet& materiaalit, const StringSet& kentat);
void askarreltavissa(const StringSet& materiaalit, const StringMapSet& reseptiluettelo,
                     const StringSet& kentat, bool bonus);
void materiaalit(const StringMapSet& reseptit, const StringSet& kentat);
bool askarreltavissa_bonus(const StringSet& materiaalit, const StringSet& tarvittavat_materiaalit,
                           const StringMapSet& reseptiluettelo);


//kayttoliittyman toteutus repun hallitsemiselle.
//ottaa parametrinaan reseptiluettelon, johon on
//luettu kayttajan valitsemasta tiedostosta halutut
//reseptit, jotka ovat tunnettuja.
void kayttoliittyma(StringMapSet& reseptiluettelo)
{
    StringSet oma_reppu;

    //rivin pitaa olla taman regexin muotoa
    const string RIVI_REGEX =
            "[[:space:]]*"      //tyhjaa
            "[[:alpha:]_]+"     //sana
            "("                 //ryhma alkaa
                "[[:space:]]+"  //tyhjaa
                "[[:alpha:]_]+" //sana
            ")*"                //ryhma loppuu (sanoja voi nyt olla maarittamaton maara)
            "[[:space:]]*";     //tyhjaa

    //rivista tunnistetaan talla sanat
    const string SANA_REGEX =
            "[[:alpha:]_]+";

    regex rivi_reg(RIVI_REGEX);
    regex sana_reg(SANA_REGEX);

    string syoterivi;

    while(cout << "testikayttoliittyma> " and getline(cin, syoterivi))
    {
        if(regex_match(syoterivi, rivi_reg))
        {
            sregex_iterator iter(syoterivi.begin(), syoterivi.end(), sana_reg);
            sregex_iterator end;

            StringSet komennot;

            while(iter != end)
            {
                string sana = iter->str();
                komennot.lisaa(sana);
                ++iter;
            }

            //kutsutaan haluttu komentofunktio kayttajan
            //syotteen perusteella tai lopetetaan heti, jos
            //komento on "loppu"
            if(komennot.at(0) == "loppu" && komennot.size() == 1)
                break;
            else if(komennot.at(0) == "reppu")
                reppu(oma_reppu, reseptiluettelo, komennot);
            else if(komennot.at(0) == "tulostareppu")
                tulostareppu(oma_reppu, komennot);
            else if(komennot.at(0) == "askarreltavissa")
                askarreltavissa(oma_reppu, reseptiluettelo, komennot, false);
            else if(komennot.at(0) == "materiaalit")
                materiaalit(reseptiluettelo, komennot);
            else if(komennot.at(0) == "askarreltavissa_bonus")
                askarreltavissa(oma_reppu, reseptiluettelo, komennot, true);
            else
                cout << "Virhe: tuntematon komento." << endl;
        }
        else
            cout << "Virhe: syotemuoto on virheellinen." << endl;
    }
}

//asettaa repun sisallon halutuksi. ottaa parametreinaan
//vanhan repun sisallon (materiaalit), tiedetyt reseptit (reseptiluettelo)
//ja kayttajan antamat komennot (kentat), joka sisaltaa tiedon
//uudesta repun sisallosta.
void reppu(StringSet& materiaalit,
           const StringMapSet& reseptiluettelo, const StringSet& kentat)
{
    //reppu pitaa aluksi tyhjentaa joka tapauksessa
    materiaalit.tyhjenna();

    for(unsigned int i = 1; i < kentat.size(); ++i)
    {
        if(reseptiluettelo.sisaltaako(kentat.at(i)))
        {
            if(materiaalit.sisaltaako(kentat.at(i)))
            {
                materiaalit.tyhjenna();
                cout << "Virhe: sama materiaali ei saa toistua." << endl;
                return;
            }
            materiaalit.lisaa(kentat.at(i));
        }
        else
        {
            materiaalit.tyhjenna();
            cout << "Virhe: tuntematon materiaali." << endl;
            return;
        }
    }

    //jos komentona on annettu pelkastaan sana "reppu"
    if(kentat.size() == 1)
        cout << "Reppu tyhjennetty." << endl;
    else
        cout << "Repun sisalto asetettu." << endl;
}

//tulostaa repun sisallon rivi kerrallaan. ottaa parametreinaan
//repun sisaltoineen (materiaalit) ja kayttajan antamat komennot (kentat)
void tulostareppu(const StringSet& materiaalit, const StringSet& kentat)
{
    if(kentat.size() != 1)
        cout << "Virhe: parametrien maara on vaara." << endl;
    else
        materiaalit.tulosta();
}

//ottaa parametreinaan repun sisaltoineen, tunnetut reseptit
//ja kayttajan syottamat kentat sekä seuraavaa funktiota
//varten tarvittavan boolin bonus.
//tulostaa tiedon siita, onko kentissa maaritelty tuote askarreltavissa
//eli onko se jo valmiina repussa tai riittavatko repussa (materiaalit)
//olevat esineet sen valmistukseen. repusta tutkitaan vain tuotteen suorat
//edeltajat jos bonus on false, ja kaikki edeltajat jos bonus on true.
//reseptiluetteloa tarvitaan tarkistukseen siita, etta tuote on ylipaataan
//olemassa/tunnettu.
void askarreltavissa(const StringSet& materiaalit,
                     const StringMapSet& reseptiluettelo, const StringSet& kentat, bool bonus)
{
    if(kentat.size() != 2)
    {
        cout << "Virhe: parametrien maara on vaara." << endl;
        return;
    }

    string tuote = kentat.at(1);

    if(!reseptiluettelo.sisaltaako(tuote))
    {
        cout << "Virhe: tuotetta ei ole olemassa." << endl;
        return;
    }

    //tuote on jo repussa
    if(materiaalit.sisaltaako(tuote))
    {
        cout << "On askarreltavissa." << endl;
        return;
    }

    StringSet tarvittavat_materiaalit = reseptiluettelo.hae_edeltajat(tuote);

    //jos edeltajia ei ole ollenkaan, on kyse tapauksesta
    //jossa on vain tuotteita, joilla ei ole edeltajia ja joita
    //ei nyt ole repussa, joten todetaan, etta ei ole askarreltavissa
    if(tarvittavat_materiaalit.size() == 0)
    {
        cout << "Ei ole askarreltavissa." << endl;
        return;
    }

    //tutkii vain suorat edeltajat, jos bonusta ei ole aktivoitu
    if(!bonus)
    {
        for(unsigned int i = 0; i < tarvittavat_materiaalit.size(); ++i)
        {
            if(!materiaalit.sisaltaako(tarvittavat_materiaalit.at(i)))
            {
                cout << "Ei ole askarreltavissa." << endl;
                return;
            }
        }
    }
    //tutkii rekursiivisesti kaikki edeltajat, jos bonus on aktivoitu.
    else if(!askarreltavissa_bonus(materiaalit, tarvittavat_materiaalit, reseptiluettelo))
    {
            cout << "Ei ole askarreltavissa." << endl;
            return;
    }

    cout << "On askarreltavissa." << endl;
}

//tutkii rekursiivisesti voidaanko parametrina annetut tarvittavat materiaalit
//loytaa tai valmistaa repussa olevista materiaaleista. mikali nykyisista
//materiaaleista ei voi valmistaa tuotetta, tutkii voiko kyseisen tuotteen
//kaikki edeltajat valmistaa. palauttaa true jos patametrin tarvittavat materiaalit
//voidaan valmistaa repun sisalloista (tai ne ovat jo siella), ja muutoin false.
//tutkii kunnes loytyy materiaali, jolla ei ole edeltajia eli tassa tilanteessa
//tuotetta ei voida valmistaa, ellei materiaalit ole siihen mennessa loytyneet repusta.
//parametrin reseptiluettelo tarvitaan halutun materiaalin edeltajien etsimiseen.
bool askarreltavissa_bonus(const StringSet& materiaalit, const StringSet& tarvittavat_materiaalit,
                           const StringMapSet& reseptiluettelo)
{
    for(unsigned int i = 0; i < tarvittavat_materiaalit.size(); ++i)
    {
        if(!materiaalit.sisaltaako(tarvittavat_materiaalit.at(i)))
        {
            StringSet alitarvittavat_materiaalit = reseptiluettelo.
                    hae_edeltajat(tarvittavat_materiaalit.at(i));

            if(alitarvittavat_materiaalit.size() == 0)
                return false;

            if(!askarreltavissa_bonus(materiaalit, alitarvittavat_materiaalit, reseptiluettelo))
                return false;
        }
    }
    return true;
}

//tulostaa halutun tuotteen rakentamiseen vaadittavat materiaalit.
//ottaa parametreinaan reseptilistan ja komentokentat, jossa toisen
//kentan on oltava haluttu tuote.
void materiaalit(const StringMapSet& reseptit, const StringSet& kentat)
{   
    if(kentat.size() != 2)
    {
        cout << "Virhe: parametrien maara on vaara." << endl;
        return;
    }

    string tuote = kentat.at(1);

    if(!reseptit.sisaltaako(tuote))
    {
        cout << "Virhe: tuotetta ei ole olemassa." << endl;
        return;
    }
    reseptit.hae_edeltajat(tuote).tulosta();
}

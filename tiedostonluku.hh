#ifndef TIEDOSTONLUKU_HH
#define TIEDOSTONLUKU_HH

#include "stringmapset.hh"

#include <iostream>
#include <fstream>
#include <regex>

bool lue_tehtavatiedosto(ifstream& tiedosto, StringMapSet& resepti);

#endif // TIEDOSTONLUKU_HH

#include "tiedostonluku.hh"

//funktio tulkitsee tehtavatiedoston rivit. tiedosto on parametrin
//ifstream-virta ja sen tiedot talletetaan toisena parametrina annettuun
//viitteeseen resepti. Jos tiedoston rivit ovat oikean muotoisia, palautetaan
//true, muutoin tiedostossa on virheellinen rivi ja palautetaan false
bool lue_tehtavatiedosto(ifstream& tiedosto, StringMapSet& resepti)
{
    //tarkistetaan regexin avulla onko syoterivi muotoa
    //tuotettava_esine:tarvittava_materiaali
    const string RIVI_REGEX =
            "[[:space:]]*"    //tyhjaa
            "[#]{0}"          //ensimmainen merkki eri kuin '#'
            "([[:alpha:]_]+)" //esine
            "[[:space:]]*"    //tyhjaa
            ":"               //kaksoispiste
            "[[:space:]]*"    //tyhjaa
            "([[:alpha:]_]+)" //materiaali
            "[[:space:]]*";   //tyhjaa

    const string KOMMENTTI_REGEX =
            "[[:space:]]*"    //tyhjaa
            "[#]{1}"          //kommenttirivin merkki
            ".*";             //mita tahansa merkkeja

    const string TYHJA_RIVI =
            "[[:space:]]*";   //tyhjaa

    regex rivi_reg(RIVI_REGEX);
    regex kommentti_reg(KOMMENTTI_REGEX);
    regex tyhja_reg(TYHJA_RIVI);
    smatch tulos;
    string rivi;

    while(getline(tiedosto, rivi))
    {
        if(regex_match(rivi, tulos, rivi_reg))
        {
            //jos lisays ei onnistunut, on kyseinen tuote:materiaali pari
            //jo olemassa reseptissa. talloin on virhetilanne ja palautetaan
            //maarittelyn mukaan false.
            if(!resepti.lisaa(tulos.str(1), tulos.str(2)))
                return false;
        }
        else if(regex_match(rivi, kommentti_reg) ||
                regex_match(rivi, tyhja_reg))
        {
            continue;
        }
        else
        {
            return false;
        }
    }
    return true;
}

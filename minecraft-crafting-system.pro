TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += C++11

SOURCES += main.cpp \
    kayttoliittyma.cpp \
    tiedostonluku.cpp \
    stringset.cpp \
    stringmapset.cpp

HEADERS += \
    kayttoliittyma.hh \
    tiedostonluku.hh \
    stringset.hh \
    stringmapset.hh

